//
//  ViewController.swift
//  sdktest
//
//  Created by Nishan Niraula on 3/28/19.
//  Copyright © 2019 eSewa. All rights reserved.
//

import UIKit
import EsewaSDK


class ViewController: UIViewController, EsewaSDKPaymentDelegate {
    
    let testButton = UIButton()
    
    var sdk: EsewaSDK!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(testButton)
        testButton.translatesAutoresizingMaskIntoConstraints = false
        testButton.backgroundColor = UIColor.darkGray
        testButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        testButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        testButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        testButton.widthAnchor.constraint(equalToConstant: 180).isActive = true
        testButton.setTitle("Test SDK", for: .normal)
        testButton.addTarget(self, action: #selector(onTestButtonTap), for: .touchUpInside)
        
        sdk = EsewaSDK(inViewController: self, environment: .development, delegate: self)
    }
    
    @objc func onTestButtonTap() {
        
        sdk.initiatePayment(merchantId: "JF0oWUE9DhUYCVM1ARVdSykNAV1fFgwdDhUYCQ==", merchantSecret: "BhwIWQQADhIYSxILExMcAgFXFhcO", productName: "Test Product", productAmount: "10", productId: "10", callbackUrl: "https://yourcallbackurl.com")
    }
    
    // MARK:- EsewaSDKPaymentDelegate

    func onEsewaSDKPaymentSuccess(info: [String : Any]) {
        // Called after payment is success
    }
    
    func onEsewaSDKPaymentError(errorDescription: String) {
        // Called after payment failed
    }
    
}

